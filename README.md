# Apple MAC Hack #

This toy enables to read some important hardware ID's of your Genuine Mac computer. It also helps to generate Apple like MAC addresses on case if somehow your board have lost those after repair service or you have to change those for some security reasons.

![apple-mac-and-guid-gen.png](https://bitbucket.org/repo/GyaExz/images/1783398431-apple-mac-and-guid-gen.png)

## Simple and easy to use ##

Apple MAC Hack is very simple and easy to use. No hassle or head heck how to read or generate some hardware ID's. Just click and voila!

## Usage instructions ##

When you open Apple MAC Hack, it shows for first your hardware ID's:

* System Serial Number
* Main Logic Board Serial Number
* ROM / MAC address
* Hardware UUID
* System UUID

You can save this data into text file by choosing from menu File -> Export

## Generate your own values ##

To generate your own MAC address, hardware UUID and system uuid just press the button "Generate" and voila, you have you own pseudo random values.

## Clover Support ##

Clover is very popular and probably the best boot loader for booting Mac OS X from custom hardware. You can predefine your hardware ID's in Clover configuration file.

Apple MAC Hack supports writing values into Clover configuration plist. Just drag your Clover configuration file into model info badge on app.

## Download ##

MAC Apple Hack is at early alpha stage at the moment. It's shared for demonstrative purposes only at the moment.

You can [download latest version of Apple MAC Hack from our site](http://applemac.tools.greenosx.com/guide/#).

## Bug report ##

Report bug etc on this bucket [Issues](https://bitbucket.org/GreenOSX/apple-mac-hack/issues) section.

## Disclaimer ##

Mac App Hack ("the software") is provided as is. Use the software at your own risk. the authors make no any other warranties whether expressed or implied. No oral or written communication from or information provided by the authors shall create a warranty. Under no circumstances shall the authors be liable for direct, indirect, special, incidental, or consequential damages resulting from the use, misuse, or inability to use the software, even if the author has been advised of the possibility of such damages. 

This software is not intended for primary usage, only for educational usage.